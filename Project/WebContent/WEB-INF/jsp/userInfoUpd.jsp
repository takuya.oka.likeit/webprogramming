<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
<div style="margin-left:auto; width:100px;">
 <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name} さん </li>
  			<li class="dropdown">
  			  <a href="Logout" class="navbar-link logout-link">ログアウト</a>
    </li>
    </ul>
    </div>
    <div style="text-align:center">
<h1>ユーザ情報更新</h1>

     <c:if test="${errMes != null}" >

		<font color = "Red">  ${errMes} </font></c:if>
<br>
<br>
<form action="UserInfoUp" method="post">
<fieldset>

ログインID <input type="text"  value="${userr.loginId}${userf[0]}" name="loginId" readonly>
<br>
<br>
パスワード <input type ="text"  name="password">
<br>
<br>
パスワード（確認）<input type ="text" name="passwordc">
<br>
<br>
ユーザ名 <input type ="text" value="${userr.name}${userf[1]}" name="name">
<br>
<br>
生年月日 <input type="text" value="${userr.birthDate}${userf[2]}" name="birthDate">
<br>
<br>
<br>
<button  type="submit" >更新</button>
</fieldset>
</form>
</div>
<br>
<br>
<br>
<a href="UserAll" >戻る</a>
</body>
</html>