<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
<div style="margin-left:auto; width:100px;">
 <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name} さん </li>
  			<li class="dropdown">
  			  <a href="Logout" class="navbar-link logout-link">ログアウト</a>
    </li>
    </ul>
    </div>
 <div class="col-12">
    <h1 class = "col-2 mx-auto">ユーザ新規登録</h1>
    <div style="text-align:center">
     <c:if test="${errMe != null}" >
	    
		<font color = "Red">  ${errMe} </font></c:if> 
     </div>
    </div>
     <br>

<br>

<form action="UserRecord" method="post">
<div class="col-12">
    <div style="margin-right:auto; margin-left:auto; width:100px;">
ログインID <input type="text" name="loginId">
<br>

<br>
パスワード　<input  type="text" name="password">
<br>
<br>
パスワード(確認)<input type="text" name="passwordc">
<br>
<br>
ユーザ名　<input type="text" name="name">
<br>
<br>
生年月日　<input type="text" name="birthDate">
    
    <br>
<br>
<button type="submit">登録</button>
    </div>
     </div>
     </form>
<br>
<br>
<br>
<a href="UserAll" >戻る</a>
</body>
</html>