<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
	crossorigin="anonymous">
</head>
<body>

	<div style="margin-left: auto; width: 100px;">
		<ul class="nav navbar-nav navbar-right">
			<li class="navbar-text">${userInfo.name}さん</li>
			<li class="dropdown"><a href="Logout" class="navbar-link logout-link">ログアウト</a></li>
		</ul>
	</div>
	<div style="text-align: center">
		<h1>ユーザ一覧</h1>
		<br> <a href="UserRecord">新規登録</a> <br> <br>
		<form action="UserAll" method="post">
			<p>
				ログインID<input type="text" name="loginId" size="40" maxlength="20">
			</p>


			<p>
				ユーザー名 <input type="text" name="name" size="40" maxlength="20">
			</p>


		<div style="margin-right: auto; margin-left: auto; width: 100px;">
			生年月日 <input type="text" name="birthDate1" class="form-control" placeholder="年/月/日"
				style="width: 150px;">
			<h6>~</h6>

			<input type="text" name="birthDate2" class="form-control" placeholder="年/月/日" style="width: 150px;">
		</div>
		<br>
		<button type="submit">検索</button>
		</form>
		<br>
		<br>
		<table class="table table-striped">
			<tr>
				<th>ログインID</th>
				<th>ユーザ名</th>
				<th>生年月日</th>
				<th></th>
			</tr>
			<!-- 初期表示 -->
			<c:if test="${userList!=null}">
			<c:forEach var="user" items="${userList}">
				<tr>
					<td>${user.loginId}</td>
					<td>${user.name}</td>
					<td>${user.birthDate}</td>

             <c:if test="${userInfo.name=='管理者'}">
					<td><a href="UserDetail?id=${user.id}"><button>参照</button></a>
						<a href="UserInfoUp?id=${user.id}"><button>更新</button></a>
								 <a href="UserDel?id=${user.id}" ><button > 削除</button></a></td>
					</c:if>
			<c:choose>
			<c:when test="${userInfo.name!='管理者' and userInfo.id!=user.id}">
			   <td><a href="UserDetail?id=${user.id}"><button>参照</button></a></td>

			   </c:when>
			<c:when test="${userInfo.name!='管理者' and userInfo.id==user.id}">
		<td><a href="UserDetail?id=${user.id}"><button>参照</button></a>
			<a href="UserInfoUp?id=${user.id}"><button>更新</button></a></td>
			</c:when>
			</c:choose>
				</tr>
			</c:forEach>
            </c:if>
            <!--  -->
            <!-- 名前のみで検索時の処理 -->
            <c:if test="${userList==null}">
			<c:forEach var="search" items="${search}">
				<tr>
					<td>${search.loginId}</td>
					<td>${search.name}</td>
					<td>${search.birthDate}</td>

             <c:if test="${userInfo.name=='管理者'}">
					<td><a href="UserDetail?id=${search.id}"><button>参照</button></a>
						<a href="UserInfoUp?id=${search.id}"><button>更新</button></a>
								 <a href="UserDel?id=${search.id}" ><button > 削除</button></a></td>
					</c:if>
			<c:choose>
			<c:when test="${userInfo.name!='管理者' and userInfo.id!=search.id}">
			   <td><a href="UserDetail?id=${search.id}"><button>参照</button></a></td>

			   </c:when>
			<c:when test="${userInfo.name!='管理者' and userInfo.id==search.id}">
		<td><a href="UserDetail?id=${search.id}"><button>参照</button></a>
			<a href="UserInfoUp?id=${search.id}"><button>更新</button></a></td>
			</c:when>
			</c:choose>
				</tr>
			</c:forEach>
            </c:if>
            
		</table>
	</div>
</body>
</html>