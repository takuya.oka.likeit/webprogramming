<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
<div style="margin-left:auto; width:100px;">
 <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name} さん </li>
  			<li class="dropdown">
  			  <a href="Logout" class="navbar-link logout-link">ログアウト</a>
    </li>
    </ul>
    </div>
    <div style="text-align:center">
    <h1>ユーザ詳細参照</h1>

<br>
ログインID
   ${use.loginId}

<br>
<br>
ユーザ名
${use.name}
<br>
<br>
生年月日
${use.birthDate}
<br>
<br>
登録日時
${use.createDate}
<br>
<br>
更新日時
${use.updateDate}
    </div>
<br>
<br>
<br>
<a href="UserAll" >戻る</a>
</body>
</html>