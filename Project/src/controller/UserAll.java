package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserAll
 */
@WebServlet("/UserAll")
public class UserAll extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAll() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("userInfo");
		if(user == null) {
		response.sendRedirect("Login");
		return;
		}
		    UserDao userDao = new UserDao();
			List<User> userList = userDao.findAll();


			request.setAttribute("userList", userList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/user all.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String name = request.getParameter("name");
        String loginId = request.getParameter("loginId");
        String birthDate1 = request.getParameter("birthDate1");
        String birthDate2 = request.getParameter("birthDate2");
		UserDao userDao = new UserDao();
		
		List<User> usersearchList = userDao.findSearch(loginId,name,birthDate1,birthDate2);
		
		request.setAttribute("search",usersearchList);
        

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/user all.jsp");
		dispatcher.forward(request, response);
	}

}
