package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserInfoUp
 */
@WebServlet("/UserInfoUp")
public class UserInfoUp extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserInfoUp() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("userInfo");
		if(user == null) {
		response.sendRedirect("Login");
		return;
		}
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		UserDao userDao = new UserDao();
		 User user3 = userDao.userInfo(id);
		 request.setAttribute("userr", user3);
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userInfoUpd.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordc = request.getParameter("passwordc");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		UserDao userDao = new UserDao();
		userDao.findAll2();

		if(password.isEmpty() == true && passwordc.isEmpty() == true && loginId.isEmpty() != true && name.isEmpty() != true && birthDate.isEmpty() != true) {
			userDao.userinfoUp2(name,birthDate,loginId);
			response.sendRedirect("UserAll");
		}else if(!(password.equals(passwordc)) || loginId.isEmpty() == true || name.isEmpty() == true || birthDate.isEmpty() == true) {
			ArrayList<String> userrr = new ArrayList<String>();

			userrr.add(loginId);
			userrr.add(name);
			userrr.add(birthDate);
			request.setAttribute("userf",userrr);

			request.setAttribute("errMes", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userInfoUpd.jsp");
			dispatcher.forward(request, response);
		} else if(password.equals(passwordc) && loginId.isEmpty() != true && name.isEmpty() != true && birthDate.isEmpty() != true){
		userDao.userinfoUp(password, name, birthDate, loginId);
		response.sendRedirect("UserAll");
	}
	}

}
