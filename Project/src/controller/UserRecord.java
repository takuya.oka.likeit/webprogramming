package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class SubUser
 */
@WebServlet("/UserRecord")
public class UserRecord extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserRecord() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("userInfo");
		if(user == null) {
		response.sendRedirect("Login");
		return;
		}


		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userRecord.jsp");
		dispatcher.forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordc = request.getParameter("passwordc");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		 UserDao userDao = new UserDao();
			User userinfo = userDao.findLoginId(loginId);

		if(!(password.equals(passwordc)) || loginId.isEmpty() == true || password.isEmpty() == true || passwordc.isEmpty() == true
				|| name.isEmpty() == true || birthDate.isEmpty() == true || userinfo != null ){

			request.setAttribute("errMe", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userRecord.jsp");
			dispatcher.forward(request, response);
		}else {

			userDao.userRecord(loginId, name, birthDate, password);


		response.sendRedirect("UserAll");
		}
}
}


