package model;
import java.io.Serializable;
import java.util.Date;
public class User implements Serializable {
    private int id;
    private String loginId;
    private String name;
    private Date birthDate;
    private String password;
    private String createDate;
    private String updateDate;


    public User(String loginId, String name) {
    	this.loginId = loginId;
    	this.name = name;
    	
    	
    }

    public User(String loginId, String name,Date birthDate,String createDate,String updateDate) {
    	this.loginId = loginId;
    	this.name = name;
    	this.birthDate = birthDate;
    	this.createDate = createDate;
    	this.updateDate = updateDate;
    }

    public User(String loginId,String name,Date birthDate,String password,String createDate,String updateDate) {
    	
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.createDate = createDate;
		this.updateDate = updateDate;
    }
    public User(int id) {
		super();
		this.id = id;
	}

	public User() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public User(int id, String loginId, String password, String name, Date birthDate) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = id;
		this.loginId = loginId;
		this.password = password;
		this.name = name;
		this.birthDate = birthDate;
	}

	public User(String loginId2) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.loginId = loginId2;
	}

	public User(int idData, String loginIdData, String nameData,Date birthDate2, String createDate2,
			String updateDate2) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = idData;
		this.loginId = loginIdData;
		this.name = nameData;
		this.birthDate = birthDate2;
		this.createDate = createDate2;
		this.updateDate = updateDate2;
	}

	public User(int id2, String loginId2, String name2,Date birthDate2, String password2, String createDate2,
			String updateDate2) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = id2;
		this.loginId = loginId2;
		this.name = name2;
		this.birthDate = birthDate2;
		this.password = password2;
		this.createDate = createDate2;
		this.updateDate = updateDate2;
	}

	public User(int idData, String loginIdData, String nameData) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = idData;
		this.loginId = loginIdData;
		this.name = nameData;
		
	}

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
}
