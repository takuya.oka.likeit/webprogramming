package dao;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;
public class UserDao {

	public User findLoginInfo(String loginId,String password) {
		Connection con = null;
		try {
			con = DBManager.getConnection();

		    String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

		    PreparedStatement pst = con.prepareStatement(sql);
		    pst.setString(1, loginId);
		    pst.setString(2,cry(password));
		    ResultSet rs = pst.executeQuery();

		    if(!rs.next()) {
		    	return null;
		    }

            int idData = rs.getInt("id");
		    String loginIdData = rs.getString("login_id");
		    String nameData = rs.getString("name");
		    return new User(idData,loginIdData,nameData);
		}catch (SQLException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } finally {

            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
	}


	public List<User> findAll(){
		Connection con = null;
		List<User> userList = new ArrayList<User>();

		try {

			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id NOT IN('admin')";
			Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);



            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
		}
	}catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return userList;


	}

	public User userInfo(String i) {
		Connection con = null;

		try {

			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE id = ?";


            PreparedStatement pst = con.prepareStatement(sql);
		    pst.setString(1, i);
		    ResultSet rs = pst.executeQuery();

		    if(!rs.next()) {
		    	return null;
		    }


            int idData = rs.getInt("id");
		    String loginIdData = rs.getString("login_id");
		    String nameData = rs.getString("name");
		    Date birthDate = rs.getDate("birth_date");
		    String passwordData = rs.getString("password");
		    String createDate = rs.getString("create_date");
            String updateDate = rs.getString("update_date");
            return new User(idData,loginIdData,nameData,birthDate,passwordData,createDate,updateDate);



	}catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }

}

		}

	public void userRecord(String loginId,String name,String birthdate,String password)  {
		Connection con = null;

		try {

			con = DBManager.getConnection();

			String sql = "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) VALUES(?,?,?,?,now(),now())";


            PreparedStatement pst = con.prepareStatement(sql);

		    pst.setString(1,loginId);
		    pst.setString(2,name);
		    pst.setString(3,birthdate);
		    pst.setString(4,cry(password));
		    pst.executeUpdate();



	}catch (SQLException | NoSuchAlgorithmException e) {
        e.printStackTrace();

    } finally {
        // データベース切断
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();

            }
        }
}

	}
	public List<User> findAll2(){
		Connection con = null;
		ArrayList<User> userList = new ArrayList<User>();

		try {

			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id ";

			Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);


            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
		}
	}catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return userList;


	}
	public void userdel(String id) {
		Connection con = null;
		try {
			con = DBManager.getConnection();
			String sql = "DELETE FROM user WHERE id = ?";


            PreparedStatement pst = con.prepareStatement(sql);
		    pst.setString(1, id);
		    pst.executeUpdate();



	}catch (SQLException e) {
        e.printStackTrace();

    } finally {
        // データベース切断
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();

            }
        }
}

	}
	public List<User> findAllLoginId(){
		Connection con = null;
		List<User> userList = new ArrayList<User>();

		try {

			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id ";

			Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);


            while (rs.next()) {
                String loginId = rs.getString("login_id");
                User user = new User(loginId);
                userList.add(user);

		}
	}catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
		return userList;
}
	public void userinfoUp(String password,String name,String birthDate,String loginId) {
		Connection con = null;

		try {

			con = DBManager.getConnection();

			String sql = "UPDATE user SET password = ?,name = ?,birth_date = ?,update_date = now() WHERE login_id = ?";

			PreparedStatement pst = con.prepareStatement(sql);
			pst.setString(1,cry(password));
			pst.setString(2,name);
			pst.setString(3,birthDate);
			pst.setString(4,loginId);
			pst.executeUpdate();

}catch (SQLException | NoSuchAlgorithmException e) {
    e.printStackTrace();

} finally {
    // データベース切断
    if (con != null) {
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }
}
}
	public void userinfoUp2(String name,String birthDate,String loginId) {
		Connection con = null;

		try {

			con = DBManager.getConnection();

			String sql = "UPDATE user SET name = ?,birth_date = ? WHERE login_id = ?";

			PreparedStatement pst = con.prepareStatement(sql);
			pst.setString(1,name);
			pst.setString(2,birthDate);
			pst.setString(3,loginId);
			pst.executeUpdate();

}catch (SQLException e) {
    e.printStackTrace();

} finally {
    // データベース切断
    if (con != null) {
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }
}
}
	public User findLoginId(String loginId) {
		Connection con = null;

		try {

			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ?";

			PreparedStatement pst = con.prepareStatement(sql);
			pst.setString(1,loginId);
			ResultSet rs = pst.executeQuery();

			if(!rs.next()) {
		    	return null;
		    }

                String loginIdd = rs.getString("login_id");
                User user = new User(loginIdd);

                return user;

	}catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

	}
	public String cry(String password) throws NoSuchAlgorithmException {

		String source = password;

		Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";

		byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		String result = DatatypeConverter.printHexBinary(bytes);

		return result;

	}
	public List<User> findByUserName(String name) {
		Connection con = null;
		List<User> userList = new ArrayList<User>();

		try {

			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE name LIKE ?";

			PreparedStatement pst = con.prepareStatement(sql);
			pst.setString(1,"%" + name + "%");
			ResultSet rs = pst.executeQuery();


			 while (rs.next()) {
				int id = rs.getInt("id");
	            String loginIdDate = rs.getString("login_id");
                String nameData = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
    		    String passwordData = rs.getString("password");
    		    String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id,loginIdDate,nameData,birthDate,passwordData,createDate,updateDate);
                userList.add(user);
			 }

	}catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
		return userList;

	}
	public List<User> findByLoginId(String loginId) {
		Connection con = null;
		List<User> userList = new ArrayList<User>();

		try {

			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ?";

			PreparedStatement pst = con.prepareStatement(sql);
			pst.setString(1,loginId);
			ResultSet rs = pst.executeQuery();

			if(!rs.next()) {
		    	return null;
		    }

				int id = rs.getInt("id");
	            String loginIdDate = rs.getString("login_id");
                String nameData = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
    		    String passwordData = rs.getString("password");
    		    String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id,loginIdDate,nameData,birthDate,passwordData,createDate,updateDate);
                userList.add(user);


	}catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
		return userList;

	}
	public List<User> findBybirthDate(String birthDate1,String birthDate2) {
		Connection con = null;
		List<User> userList = new ArrayList<User>();

		try {

			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE birth_date BETWEEN ? AND ?";

			PreparedStatement pst = con.prepareStatement(sql);
			pst.setString(1,birthDate1);
			pst.setString(2, birthDate2);
			ResultSet rs = pst.executeQuery();

			while(rs.next()) {
				int id = rs.getInt("id");
	            String loginIdDate = rs.getString("login_id");
                String nameData = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
    		    String passwordData = rs.getString("password");
    		    String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id,loginIdDate,nameData,birthDate,passwordData,createDate,updateDate);
                userList.add(user);
			}

	}catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
		return userList;

	}
	public List<User> findByUser(String loginId,String name,String birthDate1,String birthDate2) {
		Connection con = null;
		List<User> userList = new ArrayList<User>();

		try {

			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ? AND name LIKE ? AND birth_date BETWEEN ? AND ?";

			PreparedStatement pst = con.prepareStatement(sql);
			pst.setString(1,loginId);
			pst.setString(2,"%" + name + "%");
			pst.setString(3,birthDate1);
			pst.setString(4,birthDate2);
			ResultSet rs = pst.executeQuery();


			if(!rs.next()) {
		    	return null;
		    }

				int id = rs.getInt("id");
	            String loginIdDate = rs.getString("login_id");
                String nameData = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
    		    String passwordData = rs.getString("password");
    		    String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id,loginIdDate,nameData,birthDate,passwordData,createDate,updateDate);
                userList.add(user);


	}catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
		return userList;

	}
	public List<User> findSearch(String loginIdData,String nameData,String birthDate1,String birthDate2){
		Connection con = null;
		List<User> userList = new ArrayList<User>();

		try {

			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id NOT IN('admin')";
			Statement stmt = con.createStatement();


           if(!(loginIdData.isEmpty())) {
        	   sql += " AND login_id = '" + loginIdData + "'";
           }
           if(!(nameData.isEmpty())) {
        	   sql += "AND name Like '" + "%" + nameData + "%" + "'";
           }
           if(!(birthDate1.isEmpty())) {
        	   sql += "AND birth_date >= '" + birthDate1 + "'";
           }
           if(!(birthDate2.isEmpty())) {
        	   sql += "AND birth_date <= '" + birthDate2 + "'";
           }

           ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
		}
	}catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return userList;


	}

}
